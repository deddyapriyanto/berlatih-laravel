@extends('layout.master')

@section('judul')
Dashboard
@endsection

@section('subjudul')
Buat Account Baru!
@endsection

@section('content')
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First Name :</label><br><br>
        <input id="fname" type="text" name="namadepan"><br><br>
        <label for="lname" >Last Name :</label><br><br>
        <input id="lname" type="text" name="namabelakang"><br><br>

        <label>Gender :</label><br><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>

        <label for="nationality">Nationality :</label><br><br>
        <select id="nationality" name="Nationality"><br><br>
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
        </select><br><br>
    
        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="Bahasa Inggris">English<br>
        <input type="checkbox" name="Bahasa Lainnya">Other<br><br>
      
        <label>Bio :</label><br><br>
        <textarea name="bio" rows="10" cols="35"></textarea><br>
        <input type="submit" value="Sign Up">
      </form>
      @endsection