@extends('layout.master')

@section('judul')
Dashboard
@endsection

@section('subjudul')
SELAMAT DATANG! {{$namadepan}} {{$namabelakang}}
@endsection

@section('content')
    <h4>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h4>
@endsection